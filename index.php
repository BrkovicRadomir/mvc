<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


require __DIR__."/vendor/autoload.php";
define("APP_DIR", __DIR__);
$exceptionHandler = new System\Exceptions\ExceptionHandler();

set_exception_handler(function($exception) use($exceptionHandler){
    return $exceptionHandler->exception($exception);
});

$app = new System\Application();
$router = new  System\Libraries\Route\Router($app);




