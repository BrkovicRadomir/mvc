<?php


namespace System\Libraries\Database\Interfaces;


interface QueryBuilderInterface
{

    public function setTable($table);

    public function getTable();

    public function setPrimaryKey($key);

    public function getPrimaryKey();

    public function select($fields);

    public function conditional($query = "");

    public function where($field, $value, $sign = "=");

    public function orWhere($field, $value, $sign = "=");

    public function whereIn($field,  array $values);

    public function orWhereIn($field,  array $values);

    public function whereNotIn($field,  array $values);

    public function orWhereNotIn($field,  array $values);

    public function isNull($field);

    public function orIsNull($field);

    public function isNotNull($field);

    public function orIsNotNull($field);

    public function orderBy($column, $type = "ASC");

    public function limit($limit, $offset = 0);

    public function insert(array $fields);

    public function update(array $fields);

    public function get();

    public function delete();

    public function query($query);

}