<?php
/**
 * Created by PhpStorm.
 * User: rade
 * Date: 5/20/18
 * Time: 11:51 AM
 */

namespace System\Libraries\Database;


use System\Libraries\Database\Exceptions\DatabaseConfigurationExceptions;

class DataBase
{
    private $conn = null;

    /**
     * DataBase constructor.
     * @throws DatabaseConfigurationExceptions
     */
    public function __construct()
    {
        if(!$this->conn){
            global $app;

            $config= $app->config('database');

            if(!$config)
                throw  new DatabaseConfigurationExceptions("Configuration for database connection doesn't exist");


            $dsn = $config['drive'].":dbname={$config[$config['drive']]['database']};host={$config[$config['drive']]['host']}";

            try {
                $this->conn = new \PDO($dsn, $config[$config['drive']]['username'], $config[$config['drive']]['password']);
            } catch (\PDOException $e) {
                echo 'Connection failed: ' . $e->getMessage();
            }

        }

    }


    /**
     * @param $query
     * @return \PDOStatement
     */
    public function query($query){
        return $this->conn->query($query);
    }


    /**
     * @param $query
     * @param $fields
     * @param bool $update
     * @return null|\PDO|\PDOStatement
     */
    public function execute($query, $fields = null, $update = false){
        $query = $this->conn->prepare($query);
        $query->execute($fields);

        if($update)
            return $query;
        else
            return $this->conn;
    }

}