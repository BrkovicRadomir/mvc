<?php
/**
 * Created by PhpStorm.
 * User: rade
 * Date: 5/20/18
 * Time: 7:16 PM
 */

namespace System\Libraries\Database;


use System\Libraries\Database\Exceptions\IncorrectSelectTypeException;
use System\Libraries\Database\Interfaces\QueryBuilderInterface;

class QueryBuilder implements QueryBuilderInterface
{

    private $database = null;
    private $table = "";
    private $primaryKey = "";
    private $select = "*";
    private $conditional = "";
    private $orderBy = "";
    private $limit = "";

    protected $attributes = [];


    public function __construct()
    {
        if(!$this->database)
            $this->database = new DataBase();
    }

    /**
     * @param $table
     * @return $this
     */
    public function setTable($table)
    {
        $this->table = $table;
        return $this;
    }

    /**
     * @param $key
     * @return $this
     */
    public function setPrimaryKey($key)
    {
        $this->primaryKey = $key;
        return $this;
    }

    /**
     * @param $fields
     * @return $this
     * @throws IncorrectSelectTypeException
     */
    public function select($fields)
    {
        if(is_array($fields))
            $this->select = implode(',', $fields);
        elseif(is_string($fields))
            $this->select = $fields;
        else
            throw new IncorrectSelectTypeException("Your list of columns which you want to select must be array or string");


        return $this;
    }

    /**
     * @param $field
     * @param $value
     * @param string $sign
     * @return $this
     */
    public function where($field, $value, $sign = "=")
    {
        if(is_string($value))
            $value = '\''.$value.'\'';

        if ($this->conditional) {
            $this->conditional .= " AND {$field} {$sign} {$value}";
        } else
            $this->conditional = "{$field} {$sign} {$value}";

        return $this;

    }

    /**
     * @param $field
     * @param $value
     * @param string $sign
     * @return $this
     */
    public function orWhere($field, $value, $sign = "=")
    {
        if(is_string($value))
            $value = '\''.$value.'\'';

        if ($this->conditional) {
            $this->conditional .= " OR {$field} {$sign} {$value}";
        } else
            $this->conditional = "{$field} {$sign} {$value}";

        return $this;
    }

    /**
     * @param $field
     * @param array $values
     * @return $this
     */
    public function whereIn($field, array $values)
    {
        if (is_array($values) AND count($values)) {
            if ($this->conditional) {
                $this->conditional .= " AND {$field} IN ('" . implode("', '", $values) . "')";
            } else
                $this->conditional = " {$field} IN ('" . implode("', '", $values) . "')";
        }
        return $this;
    }

    /**
     * @param $field
     * @param array $values
     * @return $this
     */
    public function orWhereIn($field, array $values)
    {
        if (is_array($values) AND count($values)) {
            if ($this->conditional) {
                $this->conditional .= " OR {$field} IN ('" . implode("', '", $values) . "')";
            } else
                $this->conditional = " {$field} IN ('" . implode("', '", $values) . "')";
        }
        return $this;
    }

    /**
     * @param $field
     * @param array $values
     * @return $this
     */
    public function whereNotIn($field, array $values)
    {
        if (is_array($values) AND count($values)) {
            if ($this->conditional) {
                $this->conditional .= " AND {$field} NOT IN ('" . implode("', '", $values) . "')";
            } else
                $this->conditional = " {$field} NOT IN ('" . implode("', '", $values) . "')";

        }


        return $this;
    }

    /**
     * @param $field
     * @param array $values
     * @return $this
     */
    public function orWhereNotIn($field, array $values)
    {

        if (is_array($values) AND count($values)) {
            if ($this->conditional) {
                $this->conditional .= " OR {$field} NOT IN ('" . implode("', '", $values) . "')";
            } else
                $this->conditional = " {$field} NOT IN ('" . implode("', '", $values) . "')";

        }


        return $this;
    }

    /**
     * @param $field
     * @return $this
     */
    public function isNull($field)
    {
        if ($this->conditional) {
            $this->conditional .= " AND {$field} IS NULL";
        } else
            $this->conditional = " {$field} IS NULL";

        return $this;
    }

    /**
     * @param $field
     * @return $this
     */
    public function orIsNull($field)
    {
        if ($this->conditional) {
            $this->conditional .= " OR {$field} IS NULL";
        } else
            $this->conditional = " {$field} IS NULL";

        return $this;
    }


    /**
     * @param $field
     * @return $this
     */
    public function isNotNull($field)
    {
        if ($this->conditional) {
            $this->conditional .= " AND {$field} IS NOT NULL";
        } else
            $this->conditional = " {$field} IS NOT NULL";

        return $this;
    }

    /**
     * @param $field
     * @return $this
     */
    public function orIsNotNull($field)
    {
        if ($this->conditional) {
            $this->conditional .= " OR {$field} IS NOT NULL";
        } else
            $this->conditional = " {$field} IS NOT NULL";

        return $this;
    }

    /**
     * @param $column
     * @param string $type
     * @return $this
     */
    public function orderBy($column, $type = "ASC")
    {
        $this->orderBy = " ORDER BY {$column} {$type}";
        return $this;
    }

    /**
     * @param $limit
     * @param int $offset
     * @return $this
     */
    public function limit($limit, $offset = 0)
    {
        $this->limit = " LIMIT  {$offset}, {$limit}";
        return $this;
    }

    /**
     * @param array $fields
     * @return null|\PDO
     */
    public function insert(array $fields)
    {
        $value = [];
        $field = [];
        $i = 0;
        foreach ($fields as $key => $row){

            $value[$i] = ":".$key;
            $field[$i] = $key;

            $i++;
        }

        $sql = "INSERT INTO {$this->table} (".implode(',', $field).") VALUES (".implode(',', $value).")";

        return $this->database->execute($sql, $fields);

    }

    /**
     * @param array $fields
     * @return null|\PDO
     */
    public function update(array $fields)
    {
        $field = [];

        foreach ($fields as $key => $row) {
            $field[] = $key . " = :" . $key;
        }

        $cond = "";

        if ($this->conditional)
            $cond = "WHERE {$this->conditional}";
        elseif (isset($this->attributes[$this->primaryKey]))
            $cond = "WHERE {$this->primaryKey} = {$this->attributes[$this->primaryKey]}";


        $sql = "UPDATE {$this->table}   SET " . implode(',', $field) . " {$cond}";

        return $this->database->execute($sql, $fields, true);

    }

    /**
     * @return null|Collection
     */
    public function get()
    {
        $cond = "";

        if($this->conditional)
            $cond = " WHERE ".$this->conditional;

        $sql = "SELECT {$this->select} FROM {$this->table} {$cond} {$this->orderBy} {$this->limit} ";

        $items = $this->database->query($sql);

        if($items){
            $collection = new Collection();
            $items = $items->fetchAll(\PDO::FETCH_CLASS);

            foreach ($items as $item){
                $this->attributes = (array) $item;
                $collection->addItem($this);
            }

            return $collection;
        } else
            return null;

    }

    /**
     * Delete items from database
     */
    public function delete()
    {
        $cond = "";

        if ($this->conditional)
            $cond = "WHERE {$this->conditional}";
        elseif (isset($this->attributes[$this->primaryKey]))
            $cond = "WHERE {$this->primaryKey} = {$this->attributes[$this->primaryKey]}";


        $sql = "DELETE FROM  {$this->table}  {$cond}";

        $this->database->execute($sql);
    }

    /**
     * @param $query
     * @return null|Collection
     */
    public function query($query)
    {
        $items = $this->database->query($query);

        if($items){
            $collection = new Collection();
            $items = $items->fetchAll(\PDO::FETCH_CLASS);

            foreach ($items as $item){
                $this->attributes = (array) $item;
                $collection->addItem($this);
            }

            return $collection;
        } else
            return null;

    }


    /**
     * @param string $query
     * @return $this
     */
    public function conditional($query = "")
    {
        $this->conditional = $query;
        return $this;
    }

    /**
     * @return string
     */
    public function getTable()
    {
       return $this->table;
    }

    /**
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }
}