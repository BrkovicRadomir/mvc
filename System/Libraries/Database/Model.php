<?php

namespace System\Libraries\Database;


class Model extends QueryBuilder
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $fields
     * @return mixed
     */
    public function create($fields){
          $query = $this->insert($fields);
          return $this->find($query->lastInsertId());
    }

    /**
     * @param $primaryKey
     * @return null
     */
    public function find($primaryKey){
        $result = $this->conditional()->where($this->getPrimaryKey(), $primaryKey)->get();

        if($result and $result->count() > 0)
            return $result->result()[0];
        else
            return null;
    }


    /**
     * @param array $fields
     * @return null|\PDO
     */
    public function update(array $fields)
    {
        $update =  parent::update($fields);

        if($update->rowCount()){
            foreach ($fields as $field => $value)
                if(isset($this->attributes[$field]))
                    $this->attributes[$field] = $value;
        }

        return $update;

    }


    /**
     * @param $key
     * @return null
     */
    public function __get($key)
    {
        if (array_key_exists($key, $this->attributes)) {
            return $this->attributes[$key];
        } else
            return null;
    }




}