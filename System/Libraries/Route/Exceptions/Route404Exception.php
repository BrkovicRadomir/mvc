<?php
/**
 * Created by PhpStorm.
 * User: rade
 * Date: 5/20/18
 * Time: 12:04 PM
 */

namespace System\Libraries\Route\Exceptions;


use Throwable;

class Route404Exception extends \Exception
{

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        header("HTTP/1.0 404 Not Found");

        parent::__construct($message, $code, $previous);
    }
}