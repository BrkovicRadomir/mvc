<?php

namespace System\Libraries\Route;


use System\Application;
use System\Libraries\Route\Exceptions\Route404Exception;


class Router
{

    private $method;



    public function __construct(Application $app)
    {
        $this->setMethod();
        $this->handlingHttpRequest($app);
    }


    private function handlingHttpRequest($app){

        // show primary controller
        if(!isset($_SERVER['PATH_INFO']) and $this->method == "GET" and isset($app->config('app')['home_controller'])){
            $homeControllerNamespace = $app->config('app')['home_controller'];
            $homeController = new $homeControllerNamespace();


            if(method_exists($homeController, "index")){
                call_user_func_array([$homeController, 'index'], []);
                return true;
            } else
                throw new Route404Exception(" Route not found");




        }
        $url = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : "/";
        $url = explode('/', $url);


        // handling routes from routes.json file
        foreach ($app->config('routes') as $route){
            $routeUrl = explode('/', $route['url']);
            $passed = 0;
            $arguments = [];
            if(count($url) === count($routeUrl) and $this->method == $route['type']){
                for($i = 0; $i < count($routeUrl); $i++){
                    // compare route and url components
                    if($url[$i] == $routeUrl[$i] or ($routeUrl[$i] == "{num}" and is_numeric($url[$i])) or ($routeUrl[$i] == "{any}" and is_string($url[$i]))){
                        $passed++;
                        if($routeUrl[$i] == "{any}" or $routeUrl[$i] == "{num}"){
                            $arguments[]= $url[$i];
                        }
                    }

                }

            }


            // we found route
            if($passed == count($routeUrl)){
                $controller = new $route['controller']();
                if(method_exists($controller, $route['method'])) {
                    call_user_func_array([$controller, $route['method']], $arguments);
                    return true;
                }  else
                    throw new Route404Exception(" Route not found");
            }
        }


        if(count($url) >= 2){
            $arguments = [];
            $file = ucfirst($url[1]);
            $controller =  $app->config('app')['controllers_path'].'\\'.$file;
            $controller = new $controller();
            if(count($url) == 2){
                if(method_exists($controller, "index")) {
                    call_user_func_array([$controller, "index"], $arguments);
                    return true;
                }  else
                    throw new Route404Exception(" Route not found");
            } else {

                if(count($url) >= 3)
                   $arguments = array_splice($url, 3);


                if(method_exists($controller, $url[2])) {
                    call_user_func_array([$controller,  $url[2]], $arguments);
                    return true;
                }  else
                    throw new Route404Exception(" Route not found");
            }
        }




        throw new Route404Exception(" Route not found");

    }


    /**
     * Set type of http request
     */
    private function setMethod(){
        $this->method = isset($_POST['_method']) ? $_POST['_method'] : $_SERVER['REQUEST_METHOD'];
    }


}