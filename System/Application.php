<?php
namespace  System;

use System\Exceptions\LoadConfigException;

class Application
{
        private $config = [
            'app' => [],
            'database' => [],
            'routes' => [],
        ];

    /**
     * Application constructor.
     */
    public function __construct()
        {
            $this->loadConfigData();
        }


    /**
     * @throws LoadConfigException
     */
    private function loadConfigData(){
            $configs = scandir(APP_DIR."/config");

            foreach ($configs as $config){
                $path = APP_DIR."/config/{$config}";
                $fileInfo = pathinfo($path);
                if($fileInfo['extension'] == "json"){
                    $data = json_decode(file_get_contents($path), true);
                    if($data === null AND json_last_error() !== JSON_ERROR_NONE)
                        throw new LoadConfigException(" {$path} {$this->jsonErrorHnadler()}");

                    $this->config[$fileInfo['filename']] = $data;

                }


            }

        }


    /**
     * @param null $key
     * @return array|mixed|null
     */
    public function config($key = null){
            if($key){
                if(isset($this->config[$key]))
                    return $this->config[$key];
                else
                    return null;
            } else
                return $this->config;
        }


    /**
     * @param $data
     * @param bool $die
     */
    public function dd($data, $die = true){
            echo "<pre>";
            var_dump($data);
            echo  "</pre>";

            if($die)
                exit;
    }



    /**
     * @return string
     */
    private function  jsonErrorHnadler(){
        $response = 'No errors';
        switch (json_last_error()) {
            case JSON_ERROR_DEPTH:
                $response = 'Maximum stack depth exceeded';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $response = 'Underflow or the modes mismatch';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $response = 'Unexpected control character found';
                break;
            case JSON_ERROR_SYNTAX:
                $response = 'Syntax error, malformed JSON';
                break;
            case JSON_ERROR_UTF8:
                $response = 'Malformed UTF-8 characters, possibly incorrectly encoded';
                break;
            default:
                $response = 'Unknown error';
                break;
        }

        return $response;
    }

}