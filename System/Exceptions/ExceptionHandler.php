<?php
namespace System\Exceptions;

class ExceptionHandler
{

    public function exception($exception){
        die(get_class($exception)." :". $exception->getMessage());
    }

}