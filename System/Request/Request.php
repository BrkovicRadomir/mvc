<?php
/**
 * Created by PhpStorm.
 * User: rade
 * Date: 5/20/18
 * Time: 9:32 PM
 */

namespace System\Request;


class Request
{

    protected $bodyValues;

    public function __construct(){
        $this->bodyValues = $this->setValues();
    }


    /**
     * @return array
     */
    final private function setValues(){
        $output = [];

        $method = isset($_POST['_method']) ? $_POST['_method'] : $_SERVER['REQUEST_METHOD'];

        switch($method){
            case "GET":
                $output = $_GET;
                break;
            case "POST":
                $output = $_POST;
                break;
            default:
                parse_str(file_get_contents("php://input"), $output);


        }

        return $output;
    }


    /**
     * Return array with all variables from http request
     * @return array
     */
    public function all(){
        unset($this->bodyValues['_method']);
        unset($this->bodyValues['_token']);

        return $this->bodyValues;
    }


    /**
     * Get value of variable
     * @param $key
     * @return null
     */
    public function get($key){
        if(isset($this->bodyValues[$key]))
            return $this->bodyValues[$key];
        else
            return null;
    }


    /**
     * @return mixed
     */
    public function method(){
        return $_SERVER['REQUEST_METHOD'];
    }

}