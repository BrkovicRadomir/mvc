<?php
/**
 * Created by PhpStorm.
 * User: rade
 * Date: 5/20/18
 * Time: 8:35 PM
 */

namespace App\Models;

class User extends \System\Libraries\Database\Model
{
    public function __construct()
    {
        $this->setPrimaryKey('id');
        $this->setTable('users');
        parent::__construct();
    }

}